package com.designpattern.vivek;

public class PlanFactory {

	public Plan getPlanType(String getPlan)
	{
		if(getPlan==null)
		{
			return null;
		}
		 if(getPlan.equalsIgnoreCase("DOMESTICPLAN")) {  
             return new DomesticPlan();  
           }   
       else if(getPlan.equalsIgnoreCase("COMMERCIALPLAN")){  
            return new CommercialPlan();  
        }
		return null;   
		
	}
}
