package com.designpattern.vivek.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CustpmerDetailWithStreamAPIBySum {

	private static List<CustomerDetail> detailList=new ArrayList<>();
	public static void main(String[] args) {
		
		detailList.add(new CustomerDetail(1,"vkt","vkt@125",1f));
		detailList.add(new CustomerDetail(2,"vkt","vk@125",8f));
		detailList.add(new CustomerDetail(3,"vkt","v@125",1f));
		System.out.println("Sum of the All Price");
		double data=detailList.stream().collect(Collectors.summingDouble(detail->detail.getPrice()));
		System.out.println(data);
		System.out.println("Max or Min Prices ::");
		
		CustomerDetail bigmax=detailList.stream().max((com1,com2)->com1.getPrice()>com2.getPrice()?1:-1).get();
		System.out.println(bigmax.getEmail()+" "+bigmax.getPrice());
		
		
		System.out.println("Using Stream API which Converts   Map " );
		 Map<Integer, String> productPriceMap = detailList.stream()
				    .collect(Collectors.toMap(p -> p.getId(), p -> p.getName()));
				  System.out.println(productPriceMap);
		
		
	}
	
	/*public static void StreamAPI()
	{
		double alldata=detailList.stream().collect(Collectors.summingDouble(customer-> customer)
	}*/
	

}
