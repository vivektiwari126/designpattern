package com.designpattern.vivek.streamapi;

public class CustomerDetail {
	
	private int id;
	private String name;
	private String email;
	private float price;
	public CustomerDetail(int id, String name, String email, float price) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.price = price;
	}
	public CustomerDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	

}
