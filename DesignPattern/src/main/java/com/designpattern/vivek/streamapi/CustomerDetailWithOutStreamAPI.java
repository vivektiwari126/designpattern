package com.designpattern.vivek.streamapi;

import java.util.ArrayList;
import java.util.List;

public class CustomerDetailWithOutStreamAPI {

	 private static List < CustomerDetail > customerList = new ArrayList < CustomerDetail > ();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		customerList.add(new CustomerDetail(1,"vivek","vivektiwari.tiwari@125@gmail.com",15000f));
		customerList.add(new CustomerDetail(2,"vi","vivektiwari@125@gmail.com",12000f));
		customerList.add(new CustomerDetail(1,"vk","vivek@125@gmail.com",10000f));
		withoutStreamAPI();

	}

	
	 private static void withoutStreamAPI() {
	        // without Stream API's
	        List < Float > customerPriceList = new ArrayList < Float > ();
	        // filtering data of list
	        for (CustomerDetail product: customerList) {
	            if (product.getPrice() > 2000) {
	                // adding price to a productPriceList
	            	customerPriceList.add(product.getPrice());
	            }
	        }

	        // displaying data
	        for (Float price: customerPriceList) {
	            System.out.println(price);
	        }
	    }
	
}
