package com.designpattern.vivek.streamapi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerDetailWithStreamAPI {

	private static List<CustomerDetail> customerDetail=new ArrayList<CustomerDetail>();
	public static void main(String[] args) {

		customerDetail.add(new CustomerDetail(1,"vivek","vivektiwari.tiwari@125@gmail.com",15000f));
		customerDetail.add(new CustomerDetail(2,"vi","vivektiwari@125@gmail.com",12000f));
		customerDetail.add(new CustomerDetail(1,"vk","vivek@125@gmail.com",10000f));
		withoutStreamAPI();

	}

	public static void withoutStreamAPI() {
		
		List<Float> customerPriceList = customerDetail.stream().filter(
	 			(customerDetail1)->customerDetail1.getPrice()>12000)
				.map((customerDetail1) -> customerDetail1.getPrice()).collect(Collectors.toList());
		
		
		customerPriceList.forEach((customer)->System.out.println(customer));
		customerDetail.forEach((detail)->System.out.println(detail.getId()));
				
			
		
		
	}
}
