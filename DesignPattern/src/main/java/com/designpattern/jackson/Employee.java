package com.designpattern.jackson;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"name","id","email"})
public class Employee {
	
	private Integer id;
	private String name;
	private String email;
	private List<EmployeeCompnay> employeeCompnay;
	public Employee(Integer id, String name, String email, List<EmployeeCompnay> employeeCompnay) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.employeeCompnay = employeeCompnay;
	}
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<EmployeeCompnay> getEmployeeCompnay() {
		return employeeCompnay;
	}
	public void setEmployeeCompnay(List<EmployeeCompnay> employeeCompnay) {
		this.employeeCompnay = employeeCompnay;
	}

}
