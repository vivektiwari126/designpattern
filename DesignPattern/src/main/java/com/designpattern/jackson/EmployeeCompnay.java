package com.designpattern.jackson;

public class EmployeeCompnay {
	
	private String CompanyName;
	private Float amount;
	public EmployeeCompnay(String companyName, Float amount) {
		super();
		CompanyName = companyName;
		this.amount = amount;
	}
	public EmployeeCompnay() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	

}
