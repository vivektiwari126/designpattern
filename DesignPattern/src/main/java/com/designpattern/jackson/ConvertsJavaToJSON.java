package com.designpattern.jackson;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ConvertsJavaToJSON {

	public static void main(String[] args) throws IOException {
		
		
		ObjectMapper mapper=new ObjectMapper();
		 mapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		Employee emp=new Employee();
		emp.setId(1);
		emp.setEmail("vivek125@gmail.com");
		emp.setName("vivek");
	
		
	List<EmployeeCompnay> emp23=new ArrayList<EmployeeCompnay>();
		EmployeeCompnay empCom=new EmployeeCompnay();
		 empCom.setAmount(467477843f);
		 empCom.setCompanyName("TCS");
		 
		 EmployeeCompnay empCom1=new EmployeeCompnay();
		 empCom1.setAmount(4674843f);
		 empCom1.setCompanyName("HCL");
		 emp23.add(empCom);
		 emp23.add(empCom1);
		 
			emp.setEmployeeCompnay(emp23);
			
			
			System.out.println("-----Converting JAVA Object To JSON Object----");
			String jsonObject=mapper.writeValueAsString(emp);
			
			
			
			System.out.println(jsonObject);
			
		/*	  FileOutputStream fileOutputStream = new FileOutputStream("emp.json");
		        mapper.writeValue(fileOutputStream, emp);
		        fileOutputStream.close();
			*/
			
			
			
		/*	System.out.println("-----Converting JSON Object To JAVA  Object----");
			
			
			FileInputStream fileInputStream=new FileInputStream("emp.json");
		Employee emp123=mapper.readValue(fileInputStream, Employee.class);
		fileInputStream.close();
		
		System.out.println(emp123.getEmail()+ "\n"+emp123.getName()+"\n"+emp123.getId());
		
		for(int i=0;i<emp123.getEmployeeCompnay().size();i++)
		{
			System.out.println(emp123.getEmployeeCompnay().get(i).getCompanyName()+"\n"+emp123.getEmployeeCompnay().get(i).getAmount());
		}*/
		
		
		
		
		
			
			
			
		 

	}

}
