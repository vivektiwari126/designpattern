package com.design.http.apache;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.omg.PortableServer.ThreadPolicyOperations;

public class ApacheHttpMain {

	public static void main(String[] args) throws IOException {
		
		
		try(CloseableHttpClient httpClient=HttpClients.createDefault())
		{
			HttpGet httpGet=new HttpGet("https://www.rmlau.ac.in");
			
			
			ResponseHandler<String> responseHandler=response->
			{
				int status=response.getStatusLine().getStatusCode();
				if(status>=200&&status<300)
				{
					HttpEntity httpEntity=response.getEntity();
					
					return httpEntity!=null?EntityUtils.toString(httpEntity):null;
				}else
				{
					throw new ClientProtocolException("Unexcepted Type"+status);
				}
				
				
			};
			
			String responseBody=httpClient.execute(httpGet,responseHandler);
			
			System.out.println(responseBody);
			
			
			
		}
		// TODO Auto-generated method stub

	}

}
